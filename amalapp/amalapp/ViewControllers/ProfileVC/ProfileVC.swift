//
//  ProfileVC.swift
//  amalapp
//
//  Created by Muhammad Iqbal on 23/08/2020.
//  Copyright © 2020 Muhammad Iqbal. All rights reserved.
//

import UIKit

class ProfileVC: BaseVC {
    @IBOutlet weak var lblHeading: UILabel!
    
    @IBOutlet weak var btnProfile: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        addNavBar(isHomeVC: false)
    }


    override func setLocalization() {
        lblHeading.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "main title", comment: "")
        btnProfile.setTitle(LOCALIZARION_MANAGER.localizedStringForKey(key: "main title", comment: ""), for: .normal)
    }
    @IBAction func profileBtnPressed(_ sender: Any) {
        self.navigationController?.pushViewController(ProfileDetailVC(), animated: true)
    }
    
}
