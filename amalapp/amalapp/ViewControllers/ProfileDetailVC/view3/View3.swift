//
//  View3.swift
//  amalapp
//
//  Created by Muhammad Iqbal on 23/08/2020.
//  Copyright © 2020 Muhammad Iqbal. All rights reserved.
//

import UIKit

class View3: UIView {
    @IBOutlet weak var lbl3Value: UILabel!
    
    @IBOutlet weak var lbl2Key: UILabel!
    @IBOutlet weak var lbl2Value: UILabel!
    @IBOutlet weak var lbl1key: UILabel!
    @IBOutlet weak var lbl1Value: UILabel!
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet var contentView: UIView!
   required init?(coder aDecoder: NSCoder) {
          super.init(coder: aDecoder)
          commonInit()
      }
      
      override init(frame: CGRect) {
          super.init(frame: frame)
          commonInit()
      }
      
      func commonInit() {
          Bundle.main.loadNibNamed("View3", owner: self, options: nil)
          contentView.fixInView(self)
         
      }

    func setLocalization(){
        lblHeading.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "dawrat", comment: "")
        lbl1Value.text = "22/2/2019"
        lbl1key.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "barmajat", comment: "")
        lbl2Value.text = "22/2/2020"
        lbl2Key.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "tafiat", comment: "")
    }
}
