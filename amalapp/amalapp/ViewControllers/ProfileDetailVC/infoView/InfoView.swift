//
//  InfoView.swift
//  amalapp
//
//  Created by Muhammad Iqbal on 23/08/2020.
//  Copyright © 2020 Muhammad Iqbal. All rights reserved.
//

import UIKit

class InfoView: UIView {

    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var lblKey4Value: UILabel!
    @IBOutlet weak var lblkey4: UILabel!
    @IBOutlet weak var lblLocationKey: UILabel!
    @IBOutlet weak var lblLocationValue: UILabel!
    @IBOutlet weak var lblGenderKey: UILabel!
    @IBOutlet weak var lblGenderValue: UILabel!
    @IBOutlet weak var lblAgeKey: UILabel!
    @IBOutlet weak var lbleAgeValue: UILabel!
    @IBOutlet var contentView: UIView!
    required init?(coder aDecoder: NSCoder) {
           super.init(coder: aDecoder)
           commonInit()
       }
       
       override init(frame: CGRect) {
           super.init(frame: frame)
           commonInit()
       }
       
       func commonInit() {
           Bundle.main.loadNibNamed("InfoView", owner: self, options: nil)
           contentView.fixInView(self)
          
       }
    
    func setLocalizedValue() {
        lblAgeKey.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "age", comment: "")
        lbleAgeValue.text = "34 " + LOCALIZARION_MANAGER.localizedStringForKey(key: "years", comment: "")
        lblGenderKey.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "nationaliy", comment: "")
        lblGenderValue.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "saudi", comment: "")
        lblLocationKey.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "almadina", comment: "")
        lblLocationValue.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "riaz", comment: "")
        lblkey4.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "riaz", comment: "")
        lblKey4Value.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "riaz", comment: "")
        lblHeading.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "infoHeading", comment: "")
        
        
        
        
    }

}
