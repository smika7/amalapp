//
//  ProfileDetailVC.swift
//  amalapp
//
//  Created by Muhammad Iqbal on 23/08/2020.
//  Copyright © 2020 Muhammad Iqbal. All rights reserved.
//

import UIKit

class ProfileDetailVC: BaseVC {

    @IBOutlet weak var lblExpert: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var view6: View6!
    @IBOutlet weak var view5: View5!
    @IBOutlet weak var view4: View4!
    @IBOutlet weak var view3: View3!
    @IBOutlet weak var view2: View2!
    @IBOutlet weak var infoView: InfoView!
    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        addNavBar(isHomeVC: false)
        imageView.layer.cornerRadius = 50
        imageView.layer.borderWidth = 1.0
        imageView.layer.borderColor = UIColor.black.cgColor
    }

    override func setLocalization() {
        infoView.setLocalizedValue()
        view2.setLocaization()
        view3.setLocalization()
        view4.setLocalization()
        lblName.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "name", comment: "")
        lblExpert.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "profession", comment: "")
    }

}
