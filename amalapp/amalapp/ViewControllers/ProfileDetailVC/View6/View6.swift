//
//  View6.swift
//  amalapp
//
//  Created by Muhammad Iqbal on 24/08/2020.
//  Copyright © 2020 Muhammad Iqbal. All rights reserved.
//

import UIKit

class View6: UIView {

    @IBOutlet var contentView: UIView!
    required init?(coder aDecoder: NSCoder) {
           super.init(coder: aDecoder)
           commonInit()
       }
       
       override init(frame: CGRect) {
           super.init(frame: frame)
           commonInit()
       }
       
       func commonInit() {
           Bundle.main.loadNibNamed("View6", owner: self, options: nil)
           contentView.fixInView(self)
          
       }


}
