//
//  View4.swift
//  amalapp
//
//  Created by Muhammad Iqbal on 24/08/2020.
//  Copyright © 2020 Muhammad Iqbal. All rights reserved.
//

import UIKit

class View4: UIView {

    @IBOutlet weak var key: UILabel!
    @IBOutlet weak var value: UILabel!
    @IBOutlet weak var lbl11Value: UILabel!
    @IBOutlet weak var lbl10key: UILabel!
    @IBOutlet weak var lbl10Value: UILabel!
    @IBOutlet weak var lbl9Key: UILabel!
    @IBOutlet weak var lbl9Value: UILabel!
    @IBOutlet weak var lbl8Value: UILabel!
    @IBOutlet weak var lbl7Key: UILabel!
    @IBOutlet weak var lbl7Value: UILabel!
    @IBOutlet weak var lbl6key: UILabel!
    @IBOutlet weak var lbl6Value: UILabel!
    @IBOutlet weak var lbl5Value: UILabel!
    @IBOutlet weak var lbl4Value: UILabel!
    @IBOutlet weak var lbl4key: UILabel!
    @IBOutlet weak var lbl3Value: UILabel!
    @IBOutlet weak var lbl2Value: UILabel!
    @IBOutlet weak var lbl2Key: UILabel!
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var lbl1Value: UILabel!
    @IBOutlet weak var lbl1key: UILabel!
    @IBOutlet var contentView: UIView!
     required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            commonInit()
        }
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            commonInit()
        }
        
        func commonInit() {
            Bundle.main.loadNibNamed("View4", owner: self, options: nil)
            contentView.fixInView(self)
           
        }

    func setLocalization() {
        lblHeading.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "wazifa", comment: "")
        lbl1key.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "mobiat", comment: "")
        lbl1Value.text = "22/2/2019"
        lbl2Key.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "sharika", comment: "")
        lbl2Value.text = "22/2/2019"
        lbl3Value.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "ksa", comment: "")
        
        lbl4key.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "mobiat", comment: "")
        lbl4Value.text = "22/2/2019"
        value.text = "22/2/2019"
        key.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "sharika", comment: "")
        lbl5Value.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "ksa", comment: "")
        lbl6key.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "mobiat", comment: "")
        lbl6Value.text = "22/2/2019"
        lbl7Key.text =  LOCALIZARION_MANAGER.localizedStringForKey(key: "sharika", comment: "")
        lbl7Value.text = "22/2/2019"
        lbl8Value.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "ksa", comment: "")
        lbl9Key.text =  LOCALIZARION_MANAGER.localizedStringForKey(key: "mobiat", comment: "")
        lbl9Value.text = "22/2/2019"
        lbl10key.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "sharika", comment: "")
        lbl10Value.text = "22/2/2019"
        lbl11Value.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "ksa", comment: "")
        
        
    }
}
