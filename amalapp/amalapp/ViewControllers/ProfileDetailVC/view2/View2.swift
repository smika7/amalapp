//
//  View2.swift
//  amalapp
//
//  Created by Muhammad Iqbal on 23/08/2020.
//  Copyright © 2020 Muhammad Iqbal. All rights reserved.
//

import UIKit

class View2: UIView {
    @IBOutlet weak var lbl2key: UILabel!
    @IBOutlet weak var lbl4Key: UILabel!
    
    @IBOutlet weak var lbl6Value: UILabel!
    @IBOutlet weak var lbl5Key: UILabel!
    @IBOutlet weak var lbl4Value: UILabel!
    @IBOutlet weak var lbl5Value: UILabel!
    @IBOutlet weak var lbl3Value: UILabel!
    @IBOutlet weak var lbl2Value: UILabel!
    @IBOutlet weak var lbl1Key: UILabel!
    @IBOutlet weak var lbl1Value: UILabel!
    @IBOutlet weak var lblHeadingView2: UILabel!
    @IBOutlet var contentView: UIView!
  required init?(coder aDecoder: NSCoder) {
         super.init(coder: aDecoder)
         commonInit()
     }
     
     override init(frame: CGRect) {
         super.init(frame: frame)
         commonInit()
     }
     
     func commonInit() {
         Bundle.main.loadNibNamed("View2", owner: self, options: nil)
         contentView.fixInView(self)
        
     }


    func setLocaization(){
        lblHeadingView2.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "education", comment: "")
        lbl1Key.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "sanvi", comment: "")
        lbl1Value.text = "22/02/2019"
        lbl2key.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "markaz", comment: "")
        lbl2Value.text = "22/02/2020"
        lbl3Value.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "المملكة العربية السعودية", comment: "")
        
        lbl4Key.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "sanvi", comment: "")
        lbl4Value.text = "22/02/2019"
        lbl5Key.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "markaz", comment: "")
        lbl5Value.text = "22/02/2020"
        lbl6Value.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "المملكة العربية السعودية", comment: "")
        
        
    }
}
