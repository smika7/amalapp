//
//  NotificationVC.swift
//  amalapp
//
//  Created by Muhammad Iqbal on 22/08/2020.
//  Copyright © 2020 Muhammad Iqbal. All rights reserved.
//

import UIKit

class NotificationVC: BaseVC {

     @IBOutlet weak var tableView: UITableView!
    
     let kCellNotification = "kCellNotification"
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addNavBar(isHomeVC: false)
        tableView.register(UINib(nibName: String(describing: NotificationCell.self), bundle: Bundle.main), forCellReuseIdentifier: kCellNotification)
        tableView.rowHeight = UITableView.automaticDimension
           tableView.estimatedRowHeight = 80
     
    }


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
       
    }
   
}

extension NotificationVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: kCellNotification) as! NotificationCell
        cell.lbl1.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "dummy", comment: "")
        cell.lbl2.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "alahad", comment: "") + "  2020/3/25"
        cell.lbl3.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "detail", comment: "")
        
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
   
    
}
