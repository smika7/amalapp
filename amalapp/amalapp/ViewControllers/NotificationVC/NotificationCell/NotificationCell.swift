//
//  NotificationCell.swift
//  amalapp
//
//  Created by Muhammad Iqbal on 23/08/2020.
//  Copyright © 2020 Muhammad Iqbal. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    @IBOutlet weak var lbl3: UILabel!
    
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbl1: UILabel!
    
    var notification: Notfication? {
        didSet {
            lbl1.text = notification?.lbl1
            lbl2.text = notification?.lbl2
            lbl3.text = notification?.lbl3
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
}


struct Notfication {
    var lbl1: String?
    var lbl2: String?
    var lbl3: String?
}
