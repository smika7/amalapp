//
//  HomeVC.swift
//  amalapp
//
//  Created by Muhammad Iqbal on 21/08/2020.
//  Copyright © 2020 Muhammad Iqbal. All rights reserved.
//

import UIKit

class HomeVC: BaseVC {

    @IBOutlet weak var lblview4: UILabel!
    @IBOutlet weak var lblView3: UILabel!
    @IBOutlet weak var lblview2: UILabel!
    @IBOutlet weak var lblcount2: UILabel!
    @IBOutlet weak var lblCount1: UILabel!
    @IBOutlet weak var lblView1: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addNavBar()
        
    }

    override func setLocalization() {
        super.setLocalization()
        lblView1.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "dummy", comment: "")
        lblview2.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "dummy", comment: "")
        lblView3.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "dummy", comment: "")
        lblview4.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "dummy", comment: "")
        lblCount1.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "dummy", comment: "")
        lblcount2.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "dummy", comment: "")
        
    }
    @IBAction func profilePressed(_ sender: Any) {
        self.navigationController?.pushViewController(ProfileVC(), animated: true)
    }
    
    
}
