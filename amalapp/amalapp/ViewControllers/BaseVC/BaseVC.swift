//
//  BaseVC.swift
//  amalapp
//
//  Created by Muhammad Iqbal on 22/08/2020.
//  Copyright © 2020 Muhammad Iqbal. All rights reserved.
//

import UIKit

class BaseVC: UIViewController {

    var navBar : NavBar?
    
    override func viewDidLoad() {
        super.viewDidLoad()

       setLocalization()
    }
    
    func setLocalization() {
        
    }

    func addNavBar(isHomeVC: Bool = true) {
    
        self.navBar = NavBar.navBarView()
        self.navBar?.lblNotificationCount.addConrnerRadius(8)
        self.navBar?.lblMainTitle.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "main title", comment: "")
        self.navBar?.lblSubTitle.text = LOCALIZARION_MANAGER.localizedStringForKey(key: "sub title", comment: "")
        if !isHomeVC {
            self.navBar?.rightButton.setImage(UIImage(named: "back"), for: .normal)
            self.navBar?.lblNotificationCount.isHidden = true
            self.navBar?.leftButton.isHidden = true
            self.navBar?.lblSubTitle.isHidden = true
            self.navBar?.onNotificationBtnPressed = {
                [weak self] (button) in
                self?.navigationController?.popViewController(animated: true)
            }
        } else {
            self.navBar?.onNotificationBtnPressed = {
                [weak self] (button) in
                self?.navigationController?.pushViewController(NotificationVC(), animated: true)
                
            }
        }
       

        self.navBar?.onInfoBtnPressed = {
            [weak self] (button) in
            
        }
        
        //Adding subview with constraints
        self.navBar?.translatesAutoresizingMaskIntoConstraints = false;
        self.view.addSubview(self.navBar!);
        if #available(iOS 9.0, *) {
            self.navBar!.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor).isActive = true
            self.navBar!.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor).isActive = true
            self.navBar!.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor,constant: 30).isActive = true
            self.navBar!.heightAnchor.constraint(equalToConstant: 44.0).isActive = true
        } else {
            
            // Fallback on earlier versions
            NSLayoutConstraint(item: self.navBar!, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1.0, constant: 0).isActive = true;
            NSLayoutConstraint(item: self.navBar!, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1.0, constant: 0).isActive = true;
            NSLayoutConstraint(item: self.navBar!, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1.0, constant: 30).isActive = true;
            NSLayoutConstraint(item: self.navBar!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 44.0).isActive = true;
            
        };

    }


}
