//
//  NavBar.swift
//  amalapp
//
//  Created by Muhammad Iqbal on 22/08/2020.
//  Copyright © 2020 Muhammad Iqbal. All rights reserved.
//

import UIKit

class NavBar: UIView {

    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var lblMainTitle: UILabel!
    @IBOutlet weak var lblNotificationCount: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    
    @IBOutlet weak var rightButton: UIButton!
    var onInfoBtnPressed : ((_ button:UIButton) -> Void)?
    var onNotificationBtnPressed : ((_ button:UIButton) -> Void)?
    
    class func navBarView() -> NavBar {
        let xib = (Bundle.main.loadNibNamed(String(describing: self), owner: self, options: nil) as! Array<Any>)
        let me = xib[0] as! NavBar
        me.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 64)
        
        return me
    }
    
    
  
    @IBAction func infoBtnPressed(_ sender: UIButton) {
        onInfoBtnPressed?(sender)
    }
    
    @IBAction func notificationBtnPressed(_ sender: UIButton) {
        onNotificationBtnPressed?(sender)
    }
    
}
