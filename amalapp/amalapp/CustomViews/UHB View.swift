//
//  UHB View.swift
//  amalapp
//
//  Created by Muhammad Iqbal on 22/08/2020.
//  Copyright © 2020 Muhammad Iqbal. All rights reserved.
//

import Foundation

import UIKit

class UHBView: UIView {


    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth;
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius;
        }
    }
    
    @IBInspectable var rightTopBottomCornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = rightTopBottomCornerRadius;
            layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner]
        }
    }
    
//    @IBInspectable var leftTopCornerRadius: CGFloat = 0 {
//        didSet {
//            layer.cornerRadius = leftTopCornerRadius;
//            layer.maskedCorners = [.layerMinXMinYCorner]
//        }
//    }
//
//    @IBInspectable var rightTopCornerRadius: CGFloat = 0 {
//        didSet {
//            layer.cornerRadius = rightTopCornerRadius;
//            layer.maskedCorners = [.layerMaxXMinYCorner]
//        }
//    }
//
//    @IBInspectable var leftBottomCornerRadius: CGFloat = 0 {
//        didSet {
//            layer.cornerRadius = leftBottomCornerRadius;
//            layer.maskedCorners = [.layerMinXMaxYCorner]
//        }
//    }
//
//    @IBInspectable var rightBottomCornerRadius: CGFloat = 0 {
//        didSet {
//            layer.cornerRadius = rightBottomCornerRadius;
//            layer.maskedCorners = [.layerMaxXMaxYCorner]
//        }
//    }
    
    @IBInspectable var halfCornerRound: Bool = false {
        didSet {
            if halfCornerRound == true {
                layer.cornerRadius = frame.height * 0.5;
            }
        }
    }
    
    @IBInspectable var isCircular: Bool = false {
        didSet {
            if isCircular == true {
                layer.cornerRadius = frame.size.width / 2;
            }
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor;
        }
    }
    
    @IBInspectable var gradientColor1: UIColor = UIColor.clear {
        didSet {
            //layer.borderColor = borderColor.cgColor;
        }
    }
    
    @IBInspectable var gradientColor2: UIColor = UIColor.clear {
        didSet {
            //layer.borderColor = borderColor.cgColor;
        }
    }
    
    @IBInspectable var isGradientEnable: Bool = false {
        didSet {
            if isGradientEnable == true {
                let gradientLayer = CAGradientLayer()
                gradientLayer.frame = self.bounds
                gradientLayer.colors = [gradientColor1.cgColor,gradientColor1.cgColor, gradientColor2.cgColor]
                layer.insertSublayer(gradientLayer, at: 0)
            }
        }
    }
    
    @IBInspectable var onClick: ((UIView) -> Void)? {
        didSet {
            let gr = UITapGestureRecognizer(target: self, action: #selector(onViewTapped))
            gr.numberOfTapsRequired = 1;
            self.addGestureRecognizer(gr);
        }
    }
    
    @IBInspectable var onSwipeDown: ((UIView) -> Void)? {
        didSet {
            let gr = UISwipeGestureRecognizer(target: self, action: #selector(swipeDown))
            gr.direction = .down;
            self.addGestureRecognizer(gr);
        }
    }
    @IBInspectable var onSwipeUp: ((UIView) -> Void)? {
        didSet {
            let gr = UISwipeGestureRecognizer(target: self, action: #selector(swipeUp))
            gr.direction = .up;
            self.addGestureRecognizer(gr);
        }
    }
    @IBInspectable var onSwipeLeft: ((UIView) -> Void)? {
        didSet {
            let gr = UISwipeGestureRecognizer(target: self, action: #selector(swipeLeft))
            gr.direction = .left;
            self.addGestureRecognizer(gr);
        }
    }
    @IBInspectable var onSwipeRight: ((UIView) -> Void)? {
        didSet {
            let gr = UISwipeGestureRecognizer(target: self, action: #selector(swipeRight))
            gr.direction = .right;
            self.addGestureRecognizer(gr);
        }
    }
    
    @objc func onViewTapped()
    {
        self.onClick?(self);
    }
    @objc func swipeUp()
    {
        self.onSwipeUp?(self);
    }
    @objc func swipeLeft()
    {
        self.onSwipeLeft?(self);
    }
    @objc func swipeRight()
    {
        self.onSwipeRight?(self);
    }
    @objc func swipeDown()
    {
        self.onSwipeDown?(self);
    }
}

