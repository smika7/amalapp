//
//  UILable Extension.swift
//  amalapp
//
//  Created by Muhammad Iqbal on 22/08/2020.
//  Copyright © 2020 Muhammad Iqbal. All rights reserved.
//

import Foundation
import UIKit
extension UILabel {
    
    func addConrnerRadius(_ cornerRadius: CGFloat) {
           self.layer.cornerRadius = cornerRadius
           self.layer.masksToBounds = true
       }
}



extension String {
    
    var _localized : String {
        return self.getLocalizedTitle(title: self as NSString)
    }
    
    func getLocalizedTitle(title: NSString) -> String {
           
           let title: NSString = NSLocalizedString(title as String, tableName: "arabic",
                                                   bundle: Bundle.main,
                                                   value: title as String, comment: "") as NSString
           return title as String
       }
}


@IBDesignable extension UIButton {

    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }

    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }

    @IBInspectable var borderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
}

extension UIView {
    func fixInView(_ container: UIView!) -> Void{
        
        
        self.translatesAutoresizingMaskIntoConstraints = false;
        self.frame = container.frame;
        container.addSubview(self);
        NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .leading, multiplier: 1.0, constant: 0).isActive = true
        
        NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: container, attribute: .trailing, multiplier: 1.0, constant: 0).isActive = true
        
        NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: container, attribute: .top, multiplier: 1.0, constant: 0).isActive = true
        
        NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: container, attribute: .bottom, multiplier: 1.0, constant: 0).isActive = true
    }
}
